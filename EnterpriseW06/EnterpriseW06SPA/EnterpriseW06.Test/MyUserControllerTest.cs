﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.Results;
using EnterpriseW06.Controllers;
using EnterpriseW06.ModelDTO;
using EnterpriseW06.Models;
using NSubstitute;
using NUnit.Framework;


namespace EnterpriseW06.Test
{
    [TestFixture]
    public class MyUserControllerTest
    {
        private MyUsersController myUserController;

        [SetUp]
        public void Setup()
        {
            var data = new List<MyUser>
            {
                new MyUser()
                {
                    Address = "Test User Address",
                    Name = "John",
                    Email = "john@john.com",
                    Password = "John",
                    SecAns = "John",
                    SecQn = "John",
                    Tel = "5555555555",
                    UserId = "john"
                }
            };
            var set = Substitute.For<DbSet<MyUser>, IQueryable<MyUser>, IDbAsyncEnumerable<MyUser>>().SetupData(data);

            var context = Substitute.For<IMyUserModel>();
            context.MyUsers.Returns(set);

            myUserController = new MyUsersController(context);
        }

        [Test]
        public async Task SecurityQuestion()
        {
            var result = await myUserController.SecurityQuestion("john@john.com");

            Assert.IsInstanceOf<OkNegotiatedContentResult<MyUserResetPasswordDTO>>(result);

            var typedResult = result as OkNegotiatedContentResult<MyUserResetPasswordDTO>;
            Assert.AreEqual("john", typedResult.Content.UserId);
            Assert.AreEqual("John", typedResult.Content.SecurityQuestion);
            Assert.IsNull(typedResult.Content.SecurityAnswer);
        }

        [Test]
        public async Task UserNotFound()
        {
            var result = await myUserController.SecurityQuestion("fakeemail");

            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task ResetPassword()
        {
            var resetRequest = new MyUserResetPasswordDTO()
            {
                UserId = "john",
                SecurityQuestion = "John",
                SecurityAnswer = "John"
            };

            var result = await myUserController.ResetPassword(resetRequest);

            Assert.IsInstanceOf<OkResult>(result);
        }

        [Test]
        public async Task ResetPasswordFailed()
        {
            var resetRequest = new MyUserResetPasswordDTO()
            {
                UserId = "john",
                SecurityQuestion = "John",
                SecurityAnswer = "Doe"
            };

            var result = await myUserController.ResetPassword(resetRequest);

            Assert.IsInstanceOf<UnauthorizedResult>(result);
        }

        [Test]
        public async Task GenerateRandomString()
        {
            var passwordPattern = @"^[a-z]{6}$";
            var newPass = MyUsersController.GenerateRandomString(6);

            var match = Regex.Match(newPass, passwordPattern, RegexOptions.IgnoreCase);

            Assert.IsTrue(match.Success);
        }
    }
}
