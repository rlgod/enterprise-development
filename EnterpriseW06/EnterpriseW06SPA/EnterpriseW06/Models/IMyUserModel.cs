﻿using System.Data.Entity;
using EnterpriseW06.Components.Data;

namespace EnterpriseW06.Models
{
    public interface IMyUserModel: ISeedDbContext
    {
        DbSet<MyUser> MyUsers { get; }
    }
}
