﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using MassTransit;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using EnterpriseW06.Components.Configuration;
using EnterpriseW06.Models;
using NLog;

namespace EnterpriseW03.App_Start
{
    public static class BusConfig
    {
        public static void Register(ISettings settings, IMyUserModel _db)
        {
            var logger = LogManager.GetCurrentClassLogger();

            while (true)
            {
                var msq = new MessageQueue(settings.Get("msmqAddress"));
                ResetPasswordMessage user;
                var targetTypes = new Type[1];
                targetTypes[0] = typeof(ResetPasswordMessage);
                msq.Formatter = new XmlMessageFormatter(targetTypes);

                try
                {
                    user = (ResetPasswordMessage) (msq.Receive(new TimeSpan(0, 0, 30)).Body);
                    var myUser = (from u in _db.MyUsers
                                       where u.UserId == user.UserId
                                       select u).FirstOrDefault();

                    if (myUser != null)
                    {
                        if (!string.Equals(myUser.SecAns, user.SecurityAnswer, StringComparison.CurrentCultureIgnoreCase))
                            break;
                        myUser.Password = GenerateRandomString(6);
                    }


                    _db.SaveChangesAsync();

                    logger.Info($"New Password for user {myUser.Name} is {myUser.Password}");
                }
                catch
                {
                    continue;
                }
            }
        }


        public static string GenerateRandomString(int length)
        {
            var rand = new Random();
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var character = (char)(rand.Next() % 26 + 'A');
                stringBuilder.Append(character);
            }
            return stringBuilder.ToString();
        }
    }
}