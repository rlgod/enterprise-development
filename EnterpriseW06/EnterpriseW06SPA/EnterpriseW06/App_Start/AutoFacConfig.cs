﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac;
using Autofac.Features.Variance;
using Autofac.Integration.WebApi;
using EnterpriseW06.Components.Configuration;
using EnterpriseW06.Controllers;
using EnterpriseW06.Models;
using MediatR;
using Owin;

namespace EnterpriseW06.App_Start
{
    public static class AutoFacConfig
    {
        public static Assembly[] Assemblies { get; set; }

        static AutoFacConfig()
        {
            Assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.GetName().Name.StartsWith("EnterpriseW06")
            ).ToArray();
        }

        public static IContainer Register(ISettings settings)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            // Register my custom DbContexts
            builder.RegisterType<MyUserModel>()
                .As<IMyUserModel>()
                .WithParameter("settings", settings)
                .InstancePerLifetimeScope();

            builder.RegisterType<Settings>()
                .As<ISettings>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MyUsersController>();

            RegisterMediator(builder);
            return builder.Build();
        }

        static void RegisterMediator(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IMediator).Assembly).AsImplementedInterfaces();
            builder.Register<SingleInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });
            builder.Register<MultiInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
            });

            builder.RegisterSource(new ContravariantRegistrationSource());
            builder.RegisterAssemblyTypes(typeof(IMediator).Assembly).As<IMediator>().AsImplementedInterfaces();

            foreach (var handler in Assemblies.SelectMany(a => a.GetTypes())
                .Where(x => x.GetInterfaces().Any(i => i.IsClosedTypeOf(typeof(IAsyncRequestHandler<,>)))))
            {
                builder.RegisterType(handler)
                    .AsImplementedInterfaces()
                    .InstancePerRequest();
            }
        }
    }
}