﻿namespace EnterpriseW06.Components.Configuration
{
    /// <summary>
    /// The settings interface. Used for retrieving settings values.
    /// </summary>
    public interface ISettings
    {
        /// <summary>
        /// Attempts to retrieve a setting value or connection string value
        /// </summary>
        /// <param name="name">The name of the setting to retrieve the value for</param>
        /// <returns>The value of the setting or null</returns>
        string Get(string name);

        string GetConnectionString(string name);
    }
}