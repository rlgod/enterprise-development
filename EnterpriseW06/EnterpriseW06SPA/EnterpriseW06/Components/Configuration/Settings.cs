﻿using System.Configuration;

namespace EnterpriseW06.Components.Configuration
{
    public class Settings: ISettings
    { 
        public string Get(string name)
        {
            return ConfigurationManager.AppSettings.Get(name);
        }

        public string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}