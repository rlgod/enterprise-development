﻿using Owin;

namespace EnterpriseW06.Components.Owin
{
    public static class StaticFileServerExtension
    {
        public static IAppBuilder UseStaticFileServer(this IAppBuilder builder, string rootPath)
        {
            var mw = new StaticFileServerMiddleware(rootPath);
            return builder.Use(mw);
        }
    }
}