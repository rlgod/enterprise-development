﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EnterpriseW03;
using EnterpriseW06.Components.Configuration;
using EnterpriseW06.ModelDTO;
using EnterpriseW06.Models;
using NLog;

namespace EnterpriseW06.Controllers
{

    [RoutePrefix("api/myusers")]
    public class MyUsersController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IMyUserModel _db;
        private readonly ISettings _settings;

        public MyUsersController(IMyUserModel db, ISettings settings)
        {
            _db = db;
            _settings = settings;
        }

        [Route("securityquestion")]
        [HttpGet]
        [ResponseType(typeof(MyUserResetPasswordDTO))]
        public async Task<IHttpActionResult> SecurityQuestion(string email)
        {
            var myUser = await (from user in _db.MyUsers
                where user.Email == email
                select new MyUserResetPasswordDTO() { UserId = user.UserId, SecurityQuestion = user.SecQn }).FirstOrDefaultAsync();

            if (myUser == null)
            {
                return NotFound();
            }

            return Ok(myUser);
        }

        [Route("resetpassword")]
        [HttpPost]
        public async Task<IHttpActionResult> ResetPassword([FromBody]ResetPasswordMessage user)
        {
            var message = new Message();
            message.Body = user;
            var msmq = new MessageQueue(_settings.Get("msmqAddress"));
            msmq.Send(message);

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}