﻿using System.Configuration;

namespace EnterpriseW02.Components
{
    class Settings: ISettings
    {
        public string Get(string var)
        {
            return ConfigurationManager.AppSettings[var];
        }
    }
}
