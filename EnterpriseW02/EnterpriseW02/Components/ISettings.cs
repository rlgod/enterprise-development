﻿namespace EnterpriseW02.Components
{
    public interface ISettings
    {
        string Get(string var);
    }
}
