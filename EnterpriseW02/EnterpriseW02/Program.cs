﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using EnterpriseW02.Components;
using EnterpriseW02.Models;

namespace EnterpriseW02
{
    class Program
    {
        static void Main(string[] args)
        {
            var session = new Settings();
            var context = new MyUserContext(session);

            var user = new MyUser
            {
                Address = "11 Merrick Crt",
                Email = "dparker.tech@gmail.com",
                Name = "Daniel Parker",
                Password = "blkadf",
                SecAns = "No",
                SecQn = "Do you lift?",
                Tel = "5555555555",
                UserId = "danman"
            };

            context.Users.Add(user);

            try
            {
                var foundUser = context.Users
                    .FirstOrDefault(u => u.UserId == "danman");

                if (foundUser != null)
                {
                    context.Users.Remove(foundUser);
                }

                context.SaveChanges();
                foundUser = context.Users
                    .FirstOrDefault(u => u.UserId == "danman");

                Console.WriteLine(foundUser);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var valError in e.EntityValidationErrors.SelectMany(error => error.ValidationErrors))
                {
                    Console.WriteLine(valError.PropertyName, valError.ErrorMessage);
                }
            }
        }
    }
}
