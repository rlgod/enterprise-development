﻿using EnterpriseW02.Components;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace EnterpriseW02.Models
{
    public class MyUserContext : DbContext
    {
        public MyUserContext(ISettings session) : base(session.Get("connection"))
        {

        }

        public DbSet<MyUser> Users { get; set; }
    }

    [Table("myuser")]
    public class MyUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [MaxLength(6)]
        public string UserId { get; set; }
        [MaxLength(30)]
        public string Name { get; set; }
        [MaxLength(6)]
        public string Password { get; set; }
        [MaxLength(30)]
        public string Email { get; set; }
        [MaxLength(10)]
        public string Tel { get; set; }
        [MaxLength(30)]
        public string Address { get; set; }
        [MaxLength(60)]
        public string SecQn { get; set; }
        [MaxLength(60)]
        public string SecAns { get; set; }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            foreach (var prop in typeof (MyUser).GetProperties())
            {
                stringBuilder.Append($"{prop.Name} = {prop.GetValue(this, null)}\n");
            }

            return stringBuilder.ToString();
        }
    }
}
