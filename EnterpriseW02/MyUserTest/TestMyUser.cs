﻿using NUnit.Framework;
using EnterpriseW02.Components;
using EnterpriseW02.Models;
using NSubstitute;

namespace MyUserTest
{
    [TestFixture]
    public class TestMyUser
    {
        private readonly ISettings _settings;
        private MyUserContext _myUserContext;

        public TestMyUser()
        {
            _settings = Substitute.For<ISettings>();
            _settings.Get("connection")
                .Returns("Data Source=(localdb)\\MSSQLLocalDb;Initial Catalog=EnterpriseW02;Integrated Security=true");
        }

        [SetUp]
        public void Init()
        {
            var user = new MyUser()
            {
                Address = "11 Merrick Crt",
                Email = "dparker.tech@gmail.com",
                Name = "Daniel Parker",
                Password = "test",
                SecAns = "secAns",
                SecQn = "secQn",
                Tel = "555555555",
                UserId = "dan"
            };

            using (_myUserContext = new MyUserContext(_settings))
            {
                _myUserContext.Users.Add(user);
                _myUserContext.SaveChanges();
            }
        }

        [TearDown]
        public void Dispose()
        {
            using (_myUserContext = new MyUserContext(_settings))
            {
                _myUserContext.Database.ExecuteSqlCommand("delete from myuser");
            }
        }

        [Test]
        public void Find()
        {
            using (_myUserContext = new MyUserContext(_settings))
            {
                var user = _myUserContext.Users.Find("dan");
                Assert.NotNull(user.UserId);
            }
        }

        [Test]
        public void Delete()
        {
            using (_myUserContext = new MyUserContext(_settings))
            {
                var user = _myUserContext.Users.Find("dan");
                _myUserContext.Users.Remove(user);
                _myUserContext.SaveChanges();
                user = _myUserContext.Users.Find("dan");
                Assert.IsNull(user);
            }
        }

        [Test]
        public void Update()
        {
            using (_myUserContext = new MyUserContext(_settings))
            {
                var user = _myUserContext.Users.Find("dan");
                user.Address = "Changed";
                _myUserContext.SaveChanges();
                user = _myUserContext.Users.Find("dan");
                Assert.AreEqual(user.Address, "Changed");
            }
        }

        [Test]
        public void Insert()
        {
            var user = new MyUser()
            {
                Address = "NewTest",
                Email = "newtest",
                Name = "Daniel Parker",
                Password = "test",
                SecAns = "secAns",
                SecQn = "secQn",
                Tel = "555555555",
                UserId = "dantwo"
            };

            using (_myUserContext = new MyUserContext(_settings))
            {
                _myUserContext.Users.Add(user);
                _myUserContext.SaveChanges();

                var newUser = _myUserContext.Users.Find("dantwo");
                Assert.NotNull(newUser);
            }            
        }
    }
}
