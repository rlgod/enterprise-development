﻿(function () {
    "use strict";

    angular.module("Enterprisew03App", ["ngRoute"])
        .config(["$routeProvider", function ($routeProvider) {
            $routeProvider
                .when("/", {
                    templateUrl: "app/components/MyUser/views/ResetPassword.html",
                    controller: "MyUserCtrl"
                })
                .otherwise({
                    redirectTo: "/"
                });
        }])
        .service("MyUserService", function ($http, $q) {
            return {
                getSecurityQuestion: function(email) {
                    var q = $q.defer();

                    if (email) {
                        $http.get("/api/myusers/securityquestion", {
                            params: {
                                email: email
                            }
                        })
                            .then(function(result) {
                                q.resolve(result);
                            })
                            .catch(function(err) {
                                q.reject(err);
                            });
                    } else {
                        q.reject("No email address provided");
                    }
                
                    return q.promise;
                },

                resetPassword: function (user) {
                    var q = $q.defer();
                
                    if (user && user.securityAnswer) {
                        $http.post("/api/myusers/resetpassword", user)
                            .then(function() {
                                q.resolve("Your new password will be emailed to you.");
                            })
                            .catch(function (err) {
                                if (err.status === 401) {
                                    q.reject("You answered the security question incorrectly. Please try again");
                                } else {
                                    q.reject("Something went wrong on our end, please try again later.");
                                }
                            });
                    } else {
                        q.reject("Bad user parameter");
                    }

                    return q.promise;
                }
            }
        })
        .controller("MyUserCtrl", function($scope, $http, MyUserService) {
            $scope.user = null;
            $scope.getSecurityQuestion = getSecurityQuestion;
            $scope.resetPassword = resetPassword;
            $scope.statusMessage = {};
            $scope.showResetDialog = false;
            $scope.passwordResetSuccess = false;
            return;
            // Only func definitions beyond this point.

            function getSecurityQuestion(userEmail) {
                MyUserService.getSecurityQuestion(userEmail)
                    .then(function(result) {
                        $scope.user = result.data;
                        $scope.showResetDialog = true;
                    })
                    .catch(function(err) {

                    });
            }

            function resetPassword(user) {
                $scope.statusMessage.message = null;
                MyUserService.resetPassword(user)
                    .then(function() {
                        $scope.passwordResetSuccess = true;
                    })
                    .catch(function (err) {
                        $scope.statusMessage.message = err;
                    });
            }
        });
})();