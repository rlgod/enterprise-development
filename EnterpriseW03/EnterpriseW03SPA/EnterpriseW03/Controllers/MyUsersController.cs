﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EnterpriseW03.ModelDTO;
using EnterpriseW03.Models;
using NLog;

namespace EnterpriseW03.Controllers
{

    [RoutePrefix("api/myusers")]
    public class MyUsersController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IMyUserModel _db;

        public MyUsersController(IMyUserModel db)
        {
            _db = db;
        }

        [Route("securityquestion")]
        [HttpGet]
        [ResponseType(typeof(MyUserResetPasswordDTO))]
        public async Task<IHttpActionResult> SecurityQuestion(string email)
        {
            var myUser = await (from user in _db.MyUsers
                where user.Email == email
                select new MyUserResetPasswordDTO() { UserId = user.UserId, SecurityQuestion = user.SecQn }).FirstOrDefaultAsync();

            if (myUser == null)
            {
                return NotFound();
            }

            return Ok(myUser);
        }

        [Route("resetpassword")]
        [HttpPost]
        public async Task<IHttpActionResult> ResetPassword([FromBody]MyUserResetPasswordDTO user)
        {
            var myUser = await (from u in _db.MyUsers
                                where u.UserId == user.UserId
                                select u).FirstOrDefaultAsync();

            if (myUser == null)
            {
                return NotFound();
            }

            if (!string.Equals(myUser.SecAns, user.SecurityAnswer, StringComparison.CurrentCultureIgnoreCase))
                return Unauthorized();
            myUser.Password = GenerateRandomString(6);

            await _db.SaveChangesAsync();
            logger.Info($"New Password for user {myUser.Name} is {myUser.Password}");
            return Ok();
        }

        public static string GenerateRandomString(int length)
        {
            var rand = new Random();
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < length; i++)
            {
                var character = (char) (rand.Next()%26 + 'A');
                stringBuilder.Append(character);
            }
            return stringBuilder.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}