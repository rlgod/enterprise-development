﻿using System.Data.Entity;
using EnterpriseW03.Components.Data;

namespace EnterpriseW03.Models
{
    public interface IMyUserModel: ISeedDbContext
    {
        DbSet<MyUser> MyUsers { get; }
    }
}
