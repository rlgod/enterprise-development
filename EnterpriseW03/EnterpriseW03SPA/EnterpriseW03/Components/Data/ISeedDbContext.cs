﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace EnterpriseW03.Components.Data
{
    public interface ISeedDbContext: IDisposable, IObjectContextAdapter
    {
        DbSet<TEntity> Set<TEntity>()
            where TEntity : class;
        Task<int> SaveChangesAsync();
    }
}
