﻿using System.Configuration;

namespace EnterpriseW03.Components.Configuration
{
    public class Settings: ISettings
    { 
        public string Get(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}