﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseW01;
using EnterpriseW01.Models;
using NSubstitute;
using Xunit;
using Xunit.Sdk;

namespace EnterpriseTest
{
    public class Class1: IDisposable
    {
        private readonly ISettings _settings;
        private MyUserDAO _myUserDao;
        public Class1()
        {
            _settings = Substitute.For<ISettings>();
            _settings.Get("connection").Returns("server=(local);Initial Catalog=enterprise_w01;Integrated Security=SSPI");
            _myUserDao = new MyUserDAO(_settings);
            var newUser = new MyUser
            {
                UserId = "test15",
                Password = "test12",
                Address = "Test 1 2 3",
                Email = "dparker.tech@gmail.com",
                Name = "Namewew",
                SecAns = "Hi",
                SecQn = "Hi",
                Tel = "555555555"
            };
            _myUserDao.Save(newUser);
        }

        [Fact]
        public void TestFindAll()
        {
            var users = _myUserDao.FindAll();
            Assert.NotEmpty(users);
        }

        [Fact]
        public void TestSave()
        {
            var newUser = new MyUser
            {
                UserId = "test12",
                Password = "test12",
                Address = "Test 1 2 3",
                Email = "dparker.tech@gmail.com",
                Name = "Namewew",
                SecAns = "Hi",
                SecQn = "Hi",
                Tel = "555555555"
            };
            _myUserDao.Save(newUser);
            Assert.NotNull(_myUserDao.Find(newUser.UserId));
            _myUserDao.Delete(newUser.UserId);
        }

        [Fact]
        public void TestFind()
        {
            Assert.NotNull(_myUserDao.Find("test15"));
        }

        [Fact]
        public void TestDelete()
        {
            _myUserDao.Delete("test15");
            Assert.Null(_myUserDao.Find("test15"));
        }

        [Fact]
        public void TestUpdate()
        {
            var user = _myUserDao.Find("test15");
            user.Email = "updated@test.com";
            _myUserDao.Update(user);
            Assert.Equal(_myUserDao.Find("test15").Email, "updated@test.com");
        }

        public void Dispose()
        {
            _myUserDao.Delete("test15");
        }
    }
}
