DROP table myuser
GO

create table myuser (
	UserId nvarchar(6),
	Name nvarchar(30),
	Password nvarchar(6),
	Email nvarchar(30),
	Tel nvarchar(10),
	Address nvarchar(30),
	SecQn nvarchar(60),
	SecAns nvarchar(60),
	PRIMARY KEY(UserId)
);

GO

INSERT INTO myuser 
VALUES
('test1', 'Daniel1', 'pass1', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test2', 'Daniel2', 'pass2', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test3', 'Daniel3', 'pass3', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test4', 'Daniel4', 'pass4', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test5', 'Daniel5', 'pass5', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test6', 'Daniel6', 'pass6', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test7', 'Daniel7', 'pass7', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test8', 'Daniel8', 'pass8', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test9', 'Daniel9', 'pass9', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing'),
('test10', 'Daniel10', 'pass10', 'dparker.tech@gmail.com', '555555555', '1 Test Address Lane, Melbourne', 'What?', 'Nothing');

GO