﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseW01
{
    public interface ISettings
    {
        string Get(string setting);
    }
}
