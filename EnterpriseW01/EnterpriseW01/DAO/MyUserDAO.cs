﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using EnterpriseW01.Models;

namespace EnterpriseW01
{
    public class MyUserDAO
    {
        public string TableName;
        public string ConnectionString;
        public MyUserDAO(ISettings settings)
        {
            ConnectionString = settings.Get("connection");
        }

        public List<MyUser> FindAll()
        {
            var queryString = @"select * from myuser";
            var users = new List<MyUser>();
            SqlDataReader reader = null;
            using (var connection = new SqlConnection(ConnectionString))
            {

                var command = new SqlCommand(queryString, connection);

                try
                {
                    connection.Open();
                    reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        var user = new MyUser
                        {
                            UserId = reader["UserId"].ToString(),
                            Name = reader["Name"].ToString(),
                            Address = reader["Address"].ToString(),
                            Email = reader["Email"].ToString(),
                            Password = reader["Password"].ToString(),
                            SecAns = reader["SecAns"].ToString(),
                            SecQn = reader["SecQn"].ToString(),
                            Tel = reader["Tel"].ToString()
                        };
                        users.Add(user);
                    }
                    return users;
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }

                    connection.Close();
                }
            }
        }

        public MyUser Find(string userId)
        {
            var query = @"SELECT * FROM myuser WHERE UserId = '{0}'";
            SqlDataReader reader = null;
            MyUser user = new MyUser();
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command = new SqlCommand(string.Format(query, userId), connection);
                try
                {
                    connection.Open();
                    reader = command.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return null;
                    }

                    while (reader.HasRows && reader.Read())
                    {
                        user = new MyUser
                        {
                            UserId = reader["UserId"].ToString(),
                            Name = reader["Name"].ToString(),
                            Address = reader["Address"].ToString(),
                            Email = reader["Email"].ToString(),
                            Password = reader["Password"].ToString(),
                            SecAns = reader["SecAns"].ToString(),
                            SecQn = reader["SecQn"].ToString(),
                            Tel = reader["Tel"].ToString()
                        };
                    }

                    return user;
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }

                    connection.Close();
                }
            }
        }

        public void Update(MyUser user)
        {
            var query =
                @"UPDATE myuser
                  SET Name = '{1}', Address = '{2}', Email = '{3}', Password = '{4}', SecAns = '{5}', SecQn = '{6}', Tel = '{7}'
                  WHERE UserId = '{0}'";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command =
                    new SqlCommand(
                        string.Format(
                            query,
                            user.UserId,
                            user.Name,
                            user.Address,
                            user.Email,
                            user.Password,
                            user.SecAns,
                            user.SecQn,
                            user.Tel
                        ), connection);
                try
                {
                    connection.Open();

                    command.ExecuteNonQuery();
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void Save(MyUser user)
        {
            var query =
                @"INSERT into myuser
                  VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')";
            using (var connection = new SqlConnection(ConnectionString))
            {
                var command =
                    new SqlCommand(
                        string.Format(
                            query, 
                            user.UserId, 
                            user.Name, 
                            user.Password, 
                            user.Email, 
                            user.Tel, 
                            user.Address, 
                            user.SecQn,
                            user.SecAns
                        ), connection);
                try
                {
                    connection.Open();

                    command.ExecuteNonQuery();
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void Delete(string userId)
        {
            var query =
                @"DELETE from myuser where UserId = '{0}'";

            using (var connection = new SqlConnection(ConnectionString))
            {
                var command =
                    new SqlCommand(
                        string.Format(query, userId), connection);
                try
                {
                    connection.Open();

                    command.ExecuteNonQuery();
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
