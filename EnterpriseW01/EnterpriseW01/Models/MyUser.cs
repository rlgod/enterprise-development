﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseW01.Models
{
    public class MyUser
    {
        public string UserId;
        public string Name;
        public string Password;
        public string Email;
        public string Tel;
        public string Address;
        public string SecQn;
        public string SecAns;

        public override string ToString()
        {
            return UserId + ", " + Name + ", " + Password + ", " + Email + ", " + Tel + ", " + Address + ", " + SecQn +
                   ", " + SecAns;
        }
    }
}
