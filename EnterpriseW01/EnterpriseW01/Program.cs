﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseW01.Models;

namespace EnterpriseW01
{
    class Program
    {
        static void Main(string[] args)
        {
            var MyUserDAO = new MyUserDAO(new ApplicationSettings());
            var users = MyUserDAO.FindAll();
            Console.WriteLine("FindAll: ");
            foreach (var user in users)
            {
                Console.WriteLine(user.ToString());
            }
            Console.WriteLine();

            Console.WriteLine("Save: ");
            var newUser = new MyUser
            {
                UserId = "test12",
                Password = "test12",
                Address = "Test 1 2 3",
                Email = "dparker.tech@gmail.com",
                Name = "Namewew",
                SecAns = "Hi",
                SecQn = "Hi",
                Tel = "555555555"
            };
            MyUserDAO.Save(newUser);
            Console.WriteLine();

            var otherUser = MyUserDAO.Find("test12");
            Console.WriteLine("Find('test1'): ");
            if (otherUser != null)
            {
                Console.WriteLine(otherUser);
            }
            Console.WriteLine();

            Console.WriteLine("Update: ");
            otherUser.Password = "modpas";
            if (otherUser != null)
            {
                MyUserDAO.Update(otherUser);
                otherUser = MyUserDAO.Find(otherUser.UserId);
                Console.WriteLine(otherUser);
            }
            Console.WriteLine();

            Console.WriteLine("Delete: ");
            if (otherUser != null)
            {
                MyUserDAO.Delete(otherUser.UserId);
            }
        }
    }
}
