﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseW01
{
    class ApplicationSettings: ISettings
    {
        public string Get(string setting)
        {
            return ConfigurationManager.AppSettings[setting];
        }
    }
}
