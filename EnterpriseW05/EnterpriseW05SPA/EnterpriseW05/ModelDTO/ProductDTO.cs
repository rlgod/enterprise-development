﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriseW05.ModelDTO
{
    public class ProductDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal SellingPrice { get; set; }
        public int Quantity { get; set; }
    }
}