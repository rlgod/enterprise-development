﻿namespace EnterpriseW05.ModelDTO
{
    public class CartProductDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal SellingPrice { get; set; }
        public int OrderedQuantity { get; set; }
    }
}