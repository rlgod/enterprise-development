using Autofac;
using Owin;
using Module = Autofac.Module;

namespace EnterpriseW05
{
    public class WebModule : Module
    {
        private readonly IAppBuilder _app;

        public WebModule(IAppBuilder app)
        {
            _app = app;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
        }
    }
}
