using System.Web.Http;
using System.Web.Http.Routing;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace EnterpriseW05
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // Web API configuration and services
            var resolver = new DefaultInlineConstraintResolver();

            // Web API routes
            config.MapHttpAttributeRoutes(resolver);

            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new IsoDateTimeConverter());
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
