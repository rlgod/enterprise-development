using EnterpriseW05.Components.Configuration;
using EnterpriseW05.Components.Owin;
using Owin;

namespace EnterpriseW05
{
    public static class HostConfig
    {
        public static void Register(IAppBuilder app, ISettings settings)
        {
            var componentLocation = settings.Get("ComponentStaticFileLocation");
            var staticLocation = settings.Get("StaticFileLocation");

            if (componentLocation == staticLocation)
            {
                app.UseStaticFileServer(staticLocation);
            }
            else
            {
                app.UseStaticFileServer(componentLocation);
                app.UseStaticFileServer(staticLocation);
            }
        }
    }
}
