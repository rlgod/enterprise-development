﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Features.Variance;
using Autofac.Integration.WebApi;
using EnterpriseW05.Components.Configuration;
using EnterpriseW05.Controllers;
using EnterpriseW05.Models;
using MediatR;

namespace EnterpriseW05.App_Start
{
    public static class AutoFacConfig
    {
        public static Assembly[] Assemblies { get; set; }

        static AutoFacConfig()
        {
            Assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.GetName().Name.StartsWith("EnterpriseW05")
            ).ToArray();
        }

        public static IContainer Register(ISettings settings)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            // Register my custom DbContexts
            builder.RegisterType<ProductModel>()
                .As<IProductModel>()
                .WithParameter("settings", settings)
                .InstancePerLifetimeScope();

            builder.RegisterType<ProductController>();

            RegisterMediator(builder);
            return builder.Build();
        }

        static void RegisterMediator(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IMediator).Assembly).AsImplementedInterfaces();
            builder.Register<SingleInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });
            builder.Register<MultiInstanceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
            });

            builder.RegisterSource(new ContravariantRegistrationSource());
            builder.RegisterAssemblyTypes(typeof(IMediator).Assembly).As<IMediator>().AsImplementedInterfaces();

            foreach (var handler in Assemblies.SelectMany(a => a.GetTypes())
                .Where(x => x.GetInterfaces().Any(i => i.IsClosedTypeOf(typeof(IAsyncRequestHandler<,>)))))
            {
                builder.RegisterType(handler)
                    .AsImplementedInterfaces()
                    .InstancePerRequest();
            }
        }
    }
}