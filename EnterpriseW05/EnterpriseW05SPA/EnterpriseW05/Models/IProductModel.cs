﻿using System.Data.Entity;
using EnterpriseW05.Components.Data;

namespace EnterpriseW05.Models
{
    public interface IProductModel: ISeedDbContext
    {
        DbSet<Product> Products { get; set; }
    }
}