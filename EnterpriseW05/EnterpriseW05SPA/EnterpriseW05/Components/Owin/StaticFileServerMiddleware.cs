﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Microsoft.Owin.StaticFiles.ContentTypes;

namespace EnterpriseW05.Components.Owin
{
    public class StaticFileServerMiddleware
    {
        private readonly string _root;
        private StaticFileMiddleware _innerMiddleWare;
        private static readonly string[] ReservedExtensions = { "js", "css", "html", "jpeg", "jpg", "png", "gif", "svg", "woff", "woff2", "eof", "otf", "ttf", "map", "json" };
        private static readonly string[] ReservedPaths = { "api" };

        public StaticFileServerMiddleware(string root)
        {
            _root = root;
        }

        public void Initialize(Func<IDictionary<string, object>, Task> next)
        {
            var staticFileOptions = new StaticFileOptions {FileSystem = new PhysicalFileSystem(_root)};
            ((FileExtensionContentTypeProvider)staticFileOptions.ContentTypeProvider).Mappings.Add(".json", "applications/json");

            _innerMiddleWare = new StaticFileMiddleware(next, staticFileOptions);
        }

        public async Task Invoke(IDictionary<string, object> environment)
        {
            var context = new OwinContext(environment);
            await _innerMiddleWare.Invoke(context.Environment);

            var extension = Path.GetExtension(context.Request.Path.Value);
            if (ReservedExtensions.Any(e => ("." + e).Equals(extension, StringComparison.InvariantCultureIgnoreCase)) ||
                ReservedPaths.Any(p => context.Request.Path.StartsWithSegments(new PathString("/" + p))))
            {
                return;
            }

            var statusCode = (HttpStatusCode)context.Response.StatusCode;
            var isDefaultDocumentRequest = statusCode == HttpStatusCode.Forbidden && context.Request.Path.Value == "/";
            var isClientUri = statusCode == HttpStatusCode.NotFound || statusCode == HttpStatusCode.Forbidden;

            if (isDefaultDocumentRequest || isClientUri)
            {
                context.Request.Path = new PathString("/index.html");
                await _innerMiddleWare.Invoke(environment);
            }
        }
    }
}