﻿(function() {
    "use strict";

    angular.module("EnterpriseW05App", ["ngRoute"])
        .config([
            "$routeProvider", function($routeProvider) {
                $routeProvider
                    .when("/", {
                        templateUrl: "app/components/ShoppingCart/views/Products.html",
                        controller: "ProductCtrl"
                    })
                    .when("/cart", {
                        templateUrl: "app/components/ShoppingCart/views/Cart.html",
                        controller: "ShoppingCartCtrl"
                    })
                    .otherwise({
                        redirectTo: "/"
                    });
            }
        ])
        .service("ProductService", function($http, $q) {
            return {
                getProducts: function () {
                    var q = $q.defer();

                    $http.get("/api/product/all")
                        .then(function(result) {
                            q.resolve(result.data);
                        })
                        .catch(function() {
                            q.reject("Could not get products");
                        });

                    return q.promise;
                },
                getCart: function() {
                    var q = $q.defer();

                    $http.get("/api/shopcart")
                        .then(function(result) {
                            q.resolve(result.data);
                        })
                        .catch(function() {
                            q.reject("Error retrieving cart");
                        });

                    return q.promise;
                },
                add: function(productId, quantity) {
                    var q = $q.defer();

                    $http.put("/api/shopcart/" + productId + "/" + quantity)
                        .then(q.resolve)
                        .catch(function(err) {
                            q.reject(err.data.message);
                        });

                    return q.promise;
                },
                total: function() {
                    var q = $q.defer();

                    $http.get("/api/shopcart/total")
                        .then(function(result) {
                            q.resolve(result.data.total);
                        })
                        .catch(function() {
                            q.reject("An error occurred on the server");
                        });

                    return q.promise;
                },
                removeFromCart: function(productId) {
                    var q = $q.defer();

                    $http.delete("/api/shopcart/" + productId)
                        .then(function(result) {
                            q.resolve(result.data);
                        })
                        .catch(function(err) {
                            q.reject(err.data.message);
                        });

                    return q.promise;
                }
            }
        })
        .controller("ProductCtrl", function($scope, $timeout, ProductService) {
            $scope.products = null;
            $scope.cart = null;
            $scope.errorMessage = null;

            $scope.add = add;
            $scope.getTotal = getTotal;
            $scope.removeFromCart = removeFromCart;

            init();

            // Functions below here only
            function init() {
                $scope.$watch(function(scope) {
                    return scope.errorMessage;
                }, function() {
                    $timeout(function() {
                        $scope.errorMessage = null;
                    }, 5000);
                });
                ProductService.getProducts()
                    .then(function(products) {
                        $scope.products = products;
                    })
                    .catch(function(reason) {
                        $scope.errorMessage = reason;
                    });

                getCart();
                getTotal();


            }

            function add(productId, quantity) {
                if (quantity == 0) {
                    $scope.errorMessage = "Order quantity cannot be 0";
                    return;
                }

                console.log(productId + " " + quantity);
                ProductService.add(productId, quantity)
                    .then(function() {
                        getCart();
                        getTotal();
                    })
                    .catch(function(reason) {
                        $scope.errorMessage = reason;
                    });
            }

            function getCart() {
                ProductService.getCart()
                    .then(function (cart) {
                        $scope.cart = cart;
                    })
                    .catch(function (reason) {
                        $scope.errorMessage = reason;
                    });
            }

            function getTotal() {
                ProductService.total()
                    .then(function(total) {
                        $scope.total = total;
                    })
                    .catch(function(reason) {
                        $scope.errorMessage = reason;
                    });
            }

            function removeFromCart(productId) {
                ProductService.removeFromCart(productId)
                    .then(function(cart) {
                        $scope.cart = cart;
                        getTotal();
                    })
                    .catch(function(reason) {
                        $scope.errorMessage = reason;
                    });        
            }
        });
})();