﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EnterpriseW05.ModelDTO;
using EnterpriseW05.Models;
using NLog;

namespace EnterpriseW05.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IProductModel _db;

        public ProductController(IProductModel db)
        {
            _db = db;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllProducts()
        {
            var products = await (from p in _db.Products
                select new ProductDTO()
                {
                    Id = p.Id,
                    Name = p.Name,
                    Quantity = p.Quantity,
                    SellingPrice = p.SellingPrice
                }).ToListAsync();

            if (products == null)
            {
                return NotFound();
            }

            return Ok(products);
        }

        [Route("new")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateProduct([FromBody]Product product)
        {
            _db.Products.Add(product);
            await _db.SaveChangesAsync();

            return Ok();
        }
    }
}
