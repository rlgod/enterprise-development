﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using EnterpriseW05.ModelDTO;
using EnterpriseW05.Models;
using NLog;

namespace EnterpriseW05.Controllers
{
    [RoutePrefix("api/shopcart")]
    public class ShopCartController : ApiController
    {
        private readonly IProductModel _productDb;
        private readonly string cartName = "cartcontent";

        public ShopCartController(IProductModel productDb)
        {
            _productDb = productDb;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetCart()
        {
            var session = HttpContext.Current.Session;

            // If there's no cart, return an empty cart
            if (session?[cartName] == null)
                return Ok(new {});

            var cart = session[cartName] as Dictionary<string, CartProductDTO>;

            return Ok(cart.Values.ToList());
        }

        [Route("total")]
        [HttpGet]
        public IHttpActionResult GetTotal()
        {
            var session = HttpContext.Current.Session;

            if (session?[cartName] == null)
                return Ok(new TotalDTO
                {
                    Total = 0.0m
                });

            var cart = session[cartName] as Dictionary<string, CartProductDTO>;

            var total = cart.Values.Sum(item => (item.SellingPrice*item.OrderedQuantity));

            return Ok(new TotalDTO
            {
                Total = total
            });
        }

        [Route("{productid}/{quantity}")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateCart(string productId, int quantity)
        {
            var session = HttpContext.Current.Session;

            var product = await (from p in _productDb.Products
                where p.Id == productId
                select p).FirstOrDefaultAsync();

            if (product == null)
                return NotFound();

            if (session[cartName] == null)
            {
                session[cartName] = new Dictionary<string, CartProductDTO>();
            }

            var cart = (session[cartName] as Dictionary<string, CartProductDTO>);

            if (cart.ContainsKey(productId))
            {
                if (product.Quantity - cart[productId].OrderedQuantity - quantity < 0)
                {
                    return BadRequest("Quantity exceeded stock levels");
                }

                cart[productId].OrderedQuantity += quantity;
                return Ok();
            }

            if (product.Quantity - quantity < 0)
            {
                return BadRequest("Quantity exceeded stock levels");
            }
            
            cart.Add(productId, new CartProductDTO()
            {
                Id = product.Id,
                Name = product.Name,
                OrderedQuantity = quantity,
                SellingPrice = product.SellingPrice
            });

            return Ok();
        }

        [Route("{productId}")]
        [HttpDelete]
        public IHttpActionResult DeleteProductFromCart(string productId)
        {
            var session = HttpContext.Current.Session;

            if (session?[cartName] == null)
                return NotFound();

            var cart = session[cartName] as Dictionary<string, CartProductDTO>;

            if (!cart.ContainsKey(productId)) return NotFound();

            cart.Remove(productId);

            return Ok(cart.Values.ToList());
        }

        [Route("testsession")]
        [HttpGet]
        public IHttpActionResult TestTask()
        {
            var session = HttpContext.Current.Session;

            if (session == null) return Ok();
            session["shopwoo"] = "test";
            session["anotherthing"] = "test";

            return Ok();
        }
    }
}
