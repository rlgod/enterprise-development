using System.Data.Entity;
using System.Web.Http;
using System.Web.Http.Routing;
using Autofac.Integration.WebApi;
using EnterpriseW05.Components.Configuration;
using EnterpriseW05.App_Start;
using EnterpriseW05.Models;
using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Owin;

[assembly: OwinStartup(typeof(EnterpriseW05.Startup))]

namespace EnterpriseW05
{
    public class Startup
    {
        static Startup()
        {
           
        }

        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var settings = new Settings();

            Database.SetInitializer(new CreateDatabaseIfNotExists<ProductModel>());

            var container = AutoFacConfig.Register(settings);
            
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            var resolver = new DefaultInlineConstraintResolver();
            config.MapHttpAttributeRoutes(resolver);

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());

            app.UseWebApi(config);
            app.UseStageMarker(PipelineStage.PreHandlerExecute);
        }
    }
}
