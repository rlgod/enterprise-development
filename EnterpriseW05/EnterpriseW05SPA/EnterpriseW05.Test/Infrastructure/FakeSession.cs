﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EnterpriseW03.Test.Infrastructure
{
    public class FakeSession: HttpSessionStateBase
    {
        Dictionary<string, object> items = new Dictionary<string, object>();
  
        public override object this[string name]
        {
            get { return items.ContainsKey(name) ? items[name] : null; }
            set { items[name] = value; }
        }
    }
}
