﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Results;
using EnterpriseW05.Controllers;
using EnterpriseW05.ModelDTO;
using EnterpriseW05.Models;
using NSubstitute;
using NUnit.Framework;

namespace EnterpriseW05.Test
{
    [TestFixture]
    public class ProductControllerTest
    {
        private ProductController productController;
        private IProductModel context;

        [SetUp]
        public void Setup()
        {
            var data = new List<Product>
            {
                new Product()
                {
                    Cost = 100,
                    Id = "TV",
                    Name = "Samsung TV",
                    Quantity = 10,
                    SellingPrice = 200
                },
                new Product()
                {
                    Cost = 200,
                    Id = "TV-2",
                    Name = "LG",
                    Quantity = 10,
                    SellingPrice = 200
                }
            };
            var set = Substitute.For<DbSet<Product>, IQueryable<Product>, IDbAsyncEnumerable<Product>>().SetupData(data);

            context = Substitute.For<IProductModel>();
            context.Products.Returns(set);

            productController = new ProductController(context);

        }

        [Test]
        public async Task TestGetAll()
        {
            var result = await productController.GetAllProducts();

            Assert.IsInstanceOf<OkNegotiatedContentResult<List<ProductDTO>>>(result);

            var typedResult = result as OkNegotiatedContentResult<List<ProductDTO>>;
            Assert.AreEqual("TV", typedResult.Content[0].Id);
            Assert.AreEqual("Samsung TV", typedResult.Content[0].Name);
            Assert.AreEqual(200, typedResult.Content[0].SellingPrice);
            Assert.AreEqual(10, typedResult.Content[0].Quantity);
        }
    }
}
