﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using System.Web.SessionState;
using EnterpriseW05.Controllers;
using EnterpriseW05.ModelDTO;
using EnterpriseW05.Models;
using NSubstitute;
using NUnit.Framework;

namespace EnterpriseW03.Test
{
    [TestFixture]
    class ShoppingCartControllerTest
    {
        private ShopCartController shopCartController;
        private IProductModel context;

        [SetUp]
        public void setup()
        {
            var data = new List<Product>
            {
                new Product()
                {
                    Cost = 100,
                    Id = "TV",
                    Name = "Samsung TV",
                    Quantity = 10,
                    SellingPrice = 200
                },
                new Product()
                {
                    Cost = 200,
                    Id = "TV-2",
                    Name = "LG",
                    Quantity = 10,
                    SellingPrice = 200
                }
            };
            var set = Substitute.For<DbSet<Product>, IQueryable<Product>, IDbAsyncEnumerable<Product>>().SetupData(data);

            context = Substitute.For<IProductModel>();
            context.Products.Returns(set);

            shopCartController = new ShopCartController(context);
        }

        [Test]
        public async Task UpdateCart()
        {
            HttpContext.Current = FakeHttpContext(new Dictionary<string, object> {
                {
                    "cartcontent", new Dictionary<string, CartProductDTO>()
                }
            },"http://localhost:1234/api/product");

            var session = HttpContext.Current.Session;

            var result = await shopCartController.UpdateCart("TV", 1);

            Assert.IsInstanceOf<OkResult>(result);

            var cart = session["cartcontent"] as Dictionary<string, CartProductDTO>;

            Assert.IsInstanceOf<CartProductDTO>(cart["TV"]);
        }

        [Test]
        public void GetCart()
        {
            HttpContext.Current = FakeHttpContext(new Dictionary<string, object> {
                {
                    "cartcontent", new Dictionary<string, CartProductDTO>()
                }
            }, "http://localhost:1234/api/product");

            var cart = HttpContext.Current.Session["cartcontent"] as Dictionary<string, CartProductDTO>;
            cart.Add("TV", new CartProductDTO()
            {
                Id = "TV",
                Name = "Samsung Flatscreen",
                OrderedQuantity = 5,
                SellingPrice = 300
            });

            var result = shopCartController.GetCart();

            Assert.IsInstanceOf<OkNegotiatedContentResult<List<CartProductDTO>>>(result);

            var typedResult = result as OkNegotiatedContentResult<List<CartProductDTO>>;

            Assert.AreEqual("TV", typedResult.Content[0].Id);
            Assert.AreEqual("Samsung Flatscreen", typedResult.Content[0].Name);
            Assert.AreEqual(5, typedResult.Content[0].OrderedQuantity);
            Assert.AreEqual(300, typedResult.Content[0].SellingPrice);
        }

        [Test]
        public void GetTotal()
        {
            HttpContext.Current = FakeHttpContext(new Dictionary<string, object> {
                {
                    "cartcontent", new Dictionary<string, CartProductDTO>()
                }
            }, "http://localhost:1234/api/product");

            var cart = HttpContext.Current.Session["cartcontent"] as Dictionary<string, CartProductDTO>;
            cart.Add("TV", new CartProductDTO
            {
                Id = "TV",
                Name = "Samsung TV",
                OrderedQuantity = 1,
                SellingPrice = 200
            });

            cart.Add("TV-2", new CartProductDTO
            {
                Id = "TV",
                Name = "Samsung Flatscreen",
                OrderedQuantity = 5,
                SellingPrice = 300
            });

            var result = shopCartController.GetTotal();

            Assert.IsInstanceOf<OkNegotiatedContentResult<TotalDTO>>(result);

            var typedResult = result as OkNegotiatedContentResult<TotalDTO>;

            Assert.AreEqual(1700, typedResult.Content.Total);
        }

        [Test]
        public void DeleteProductFromCart()
        {
            HttpContext.Current = FakeHttpContext(new Dictionary<string, object> {
                {
                    "cartcontent", new Dictionary<string, CartProductDTO>()
                }
            }, "http://localhost:1234/api/product");

            var cart = HttpContext.Current.Session["cartcontent"] as Dictionary<string, CartProductDTO>;
            cart.Add("TV", new CartProductDTO
            {
                Id = "TV",
                Name = "Samsung TV",
                OrderedQuantity = 1,
                SellingPrice = 200
            });

            cart.Add("TV-2", new CartProductDTO
            {
                Id = "TV-2",
                Name = "Samsung Flatscreen",
                OrderedQuantity = 5,
                SellingPrice = 300
            });

            var result = shopCartController.DeleteProductFromCart("TV");

            Assert.IsInstanceOf<OkNegotiatedContentResult<List<CartProductDTO>>>(result);

            var typedResult = result as OkNegotiatedContentResult<List<CartProductDTO>>;

            Assert.IsFalse(typedResult.Content.Exists(c => c.Id == "TV"));
        }

        public HttpContext FakeHttpContext(Dictionary<string, object> sessionVariables, string path)
        {
            var httpRequest = new HttpRequest(string.Empty, path, string.Empty);
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);
            httpContext.User = new GenericPrincipal(new GenericIdentity("username"), new string[0]);
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("username"), new string[0]);
            var sessionContainer = new HttpSessionStateContainer(
              "id",
              new SessionStateItemCollection(),
              new HttpStaticObjectsCollection(),
              10,
              true,
              HttpCookieMode.AutoDetect,
              SessionStateMode.InProc,
              false);

            foreach (var var in sessionVariables)
            {
                sessionContainer.Add(var.Key, var.Value);
            }

            SessionStateUtility.AddHttpSessionStateToContext(httpContext, sessionContainer);
            return httpContext;
        }
    }
}
