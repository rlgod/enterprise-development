﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.Results;
using EnterpriseW04.Controllers;
using EnterpriseW04.ModelDTO;
using EnterpriseW04.Models;
using NSubstitute;
using NUnit.Framework;


namespace EnterpriseW04.Test
{
    [TestFixture]
    public class MyUserControllerTest
    {
        private MyUsersController myUserController;
        private IMyUserModel context;

        [SetUp]
        public void Setup()
        {
            var data = new List<MyUser>
            {
                new MyUser()
                {
                    Address = "Test User Address",
                    Name = "John",
                    Email = "john@john.com",
                    Password = "John",
                    SecAns = "John",
                    SecQn = "John",
                    Tel = "5555555555",
                    UserId = "john"
                }
            };
            var set = Substitute.For<DbSet<MyUser>, IQueryable<MyUser>, IDbAsyncEnumerable<MyUser>>().SetupData(data);

            context = Substitute.For<IMyUserModel>();
            context.MyUsers.Returns(set);

            myUserController = new MyUsersController(context);
        }

        [Test]
        public async Task SecurityQuestion()
        {
            var result = await myUserController.SecurityQuestion("john@john.com");

            Assert.IsInstanceOf<OkNegotiatedContentResult<MyUserResetPasswordDTO>>(result);

            var typedResult = result as OkNegotiatedContentResult<MyUserResetPasswordDTO>;
            Assert.AreEqual("john", typedResult.Content.UserId);
            Assert.AreEqual("John", typedResult.Content.SecurityQuestion);
            Assert.IsNull(typedResult.Content.SecurityAnswer);
        }

        [Test]
        public async Task UserNotFound()
        {
            var result = await myUserController.SecurityQuestion("fakeemail");

            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task ResetPassword()
        {
            var resetRequest = new MyUserResetPasswordDTO()
            {
                UserId = "john",
                SecurityQuestion = "John",
                SecurityAnswer = "John",
                NewPassword = "new"
            };

            var result = await myUserController.ResetPassword(resetRequest);

            Assert.IsInstanceOf<OkResult>(result);
            Assert.AreEqual("new", context.MyUsers.FirstOrDefault().Password);
        }

        [Test]
        public async Task ResetPasswordFailed()
        {
            var resetRequest = new MyUserResetPasswordDTO()
            {
                UserId = "john",
                SecurityQuestion = "John",
                SecurityAnswer = "Doe",
                NewPassword = "new"
            };

            var result = await myUserController.ResetPassword(resetRequest);

            Assert.IsInstanceOf<UnauthorizedResult>(result);
        }
    }
}
