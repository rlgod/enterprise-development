﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using NSubstitute;
using Ploeh.AutoFixture.Kernel;

namespace EnterpriseW04.Test.Infrastructure
{
    public class DbSetCustomization<TModel> : ISpecimenBuilder where TModel : class
    {
        private readonly IQueryable<TModel> _items;

        public DbSetCustomization(IQueryable<TModel> items)
        {
            _items = items;
        }

        public object Create(object request, ISpecimenContext context)
        {
            var requestType = request as Type;
            if (requestType == null)
            {
                return new NoSpecimen(request);
            }

            if (!typeof(IDbSet<TModel>).IsAssignableFrom(requestType))
            {
                return new NoSpecimen(request);
            }

            var userSet = Substitute.For<IDbSet<TModel>, IDbAsyncEnumerable<TModel>>();
            userSet.Initialize(_items);

            return userSet;
        }
    }
}
