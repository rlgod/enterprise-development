using EnterpriseW04.Components.Configuration;
using EnterpriseW04.Components.Owin;
using Owin;

namespace EnterpriseW04
{
    public static class HostConfig
    {
        public static void Register(IAppBuilder app, ISettings settings)
        {
            var componentLocation = settings.Get("ComponentStaticFileLocation");
            var staticLocation = settings.Get("StaticFileLocation");

            if (componentLocation == staticLocation)
            {
                app.UseStaticFileServer(staticLocation);
            }
            else
            {
                app.UseStaticFileServer(componentLocation);
                app.UseStaticFileServer(staticLocation);
            }
        }
    }
}
