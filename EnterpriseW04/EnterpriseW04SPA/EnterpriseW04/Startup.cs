using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Routing;
using Autofac;
using Autofac.Features.Variance;
using Autofac.Integration.WebApi;
using EnterpriseW04.App_Start;
using EnterpriseW04.Components.Configuration;
using EnterpriseW04.Controllers;
using EnterpriseW04.Models;
using MediatR;
using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Owin;

[assembly: OwinStartup(typeof(EnterpriseW04.Startup))]

namespace EnterpriseW04
{
    public class Startup
    {
        static Startup()
        {
           
        }

        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var settings = new Settings();

            var container = AutoFacConfig.Register(settings);
            
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            var resolver = new DefaultInlineConstraintResolver();
            config.MapHttpAttributeRoutes(resolver);

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());

            app.UseWebApi(config);
            app.UseStageMarker(PipelineStage.PreHandlerExecute);
        }
    }
}
