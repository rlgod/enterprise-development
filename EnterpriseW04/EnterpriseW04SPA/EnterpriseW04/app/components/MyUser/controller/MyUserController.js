﻿(function () {
    "use strict";

    angular.module("EnterpriseW04App", ["ngRoute"])
        .config([
            "$routeProvider", function($routeProvider) {
                $routeProvider
                    .when("/", {
                        templateUrl: "app/components/MyUser/views/ResetPassword.html",
                        controller: "MyUserCtrl"
                    })
                    .when("/result", {
                        templateUrl: "app/components/MyUser/views/ResetResult.html"
                    })
                    .otherwise({
                        redirectTo: "/"
                    });
            }
        ])
        .service("MyUserService", function($http, $q) {
            return {
                getSecurityQuestion: function(email) {
                    var q = $q.defer();

                    if (email) {
                        $http.get("/api/myusers/securityquestion", {
                                params: {
                                    email: email
                                }
                            })
                            .then(function(result) {
                                q.resolve(result);
                            })
                            .catch(function(err) {
                                q.reject("A user with that email address was not found");
                            });
                    } else {
                        q.reject("No email address provided");
                    }

                    return q.promise;
                },

                resetPassword: function(user) {
                    var q = $q.defer();

                    if (user && user.securityAnswer && user.newPassword) {
                        $http.post("/api/myusers/resetpassword", user)
                            .then(function() {
                                q.resolve("Password was successfully changed");
                            })
                            .catch(function(err) {
                                if (err.status === 401) {
                                    q.reject("You answered the security question incorrectly. Please try again");
                                } else if (err.status === 304) {
                                    q.reject("Your new password cannot be empty");
                                } else {
                                    q.reject("Something went wrong on our end, please try again later.");
                                }
                            });
                    } else {
                        q.reject("You must complete all the fields");
                    }

                    return q.promise;
                }
            }
        })
        .controller("MyUserCtrl", function($scope, $http, $location, MyUserService) {
            $scope.getSecurityQuestion = getSecurityQuestion;
            $scope.resetPassword = resetPassword;
            $scope.resetForm = resetForm;
            $scope.init = init;
            $scope.init();
            return;

            // Only func definitions beyond this point.

            function init() {
                $scope.user = {};
                $scope.statusMessage = {};
                $scope.showResetDialog = false;
                $scope.passwordResetSuccess = false;
            }

            function getSecurityQuestion(userEmail) {
                MyUserService.getSecurityQuestion(userEmail)
                    .then(function(result) {
                        $scope.user = result.data;
                        $scope.showResetDialog = true;
                        $scope.statusMessage.message = null;
                    })
                    .catch(function(err) {
                        $scope.statusMessage.message = err;
                        $scope.user = null;
                        $scope.showResetDialog = false;
                    });
            }

            function resetPassword(user) {
                $scope.statusMessage.message = null;
                MyUserService.resetPassword(user)
                    .then(function() {
                        $location.path("result");
                    })
                    .catch(function(err) {
                        $scope.statusMessage.message = err;
                    });
            }

            function resetForm() {
                $scope.init();
            }
        });
})();