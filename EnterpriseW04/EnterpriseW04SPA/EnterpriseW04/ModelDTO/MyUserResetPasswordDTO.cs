﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriseW04.ModelDTO
{
    public class MyUserResetPasswordDTO
    {
        public string UserId { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }
        public string NewPassword { get; set; }
    }
}