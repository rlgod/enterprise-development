using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnterpriseW04.Components.Configuration;

namespace EnterpriseW04.Models
{
    using System.Data.Entity;

    public class MyUserModel : DbContext, IMyUserModel
    {
        // Your context has been configured to use a 'MyUserModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'EnterpriseW04.Models.MyUserModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'MyUserModel' 
        // connection string in the application configuration file.

        public MyUserModel(ISettings settings)
            : base(settings.Get("MyUserModel"))
        {
        }

        public virtual DbSet<MyUser> MyUsers { get; set; }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    public class MyUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [MaxLength(6)]
        public string UserId { get; set; }
        [MaxLength(30)]
        public string Name { get; set; }
        [MaxLength(6)]
        public string Password { get; set; }
        [MaxLength(30)]
        public string Email { get; set; }
        [MaxLength(10)]
        public string Tel { get; set; }
        [MaxLength(30)]
        public string Address { get; set; }
        [MaxLength(60)]
        public string SecQn { get; set; }
        [MaxLength(60)]
        public string SecAns { get; set; }
    }
}