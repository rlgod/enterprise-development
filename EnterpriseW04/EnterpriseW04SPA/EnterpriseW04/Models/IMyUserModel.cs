﻿using System.Data.Entity;
using EnterpriseW04.Components.Data;

namespace EnterpriseW04.Models
{
    public interface IMyUserModel: ISeedDbContext
    {
        DbSet<MyUser> MyUsers { get; }
    }
}
